{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ./wm/xmonad.nix
    ];
 
####################################################################################################################### 

  boot.kernelParams = [ "intel_pstate=no_hwp" ];

  fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];

  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "nodev";
    efiSupport = true;
  };
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.luks.devices.luksroot = {
    device = "/dev/disk/by-uuid/8f52dba1-b45d-4ea9-8421-212ed7bd49d7";
    preLVM = true;
    allowDiscards = true;
  };

  console.keyMap = "de-latin1";

  time.timeZone = "Europe/Berlin";

  i18n = {
    defaultLocale = "en_GB.UTF-8";
  };

  sound = {
    enable = true;
    mediaKeys.enable = true;
  };
  hardware.pulseaudio.enable = true;

  networking.wireless.iwd.enable = true;

####################################################################################################################### 

  users.extraUsers.kolja = {
    name = "kolja";
    group = "users";
    extraGroups = [
      "wheel"
      "disk"
      "audio"
      "video"
      "networkmanager"
      "systemd-journal"
    ];
    createHome = true;
    uid =  1000;
    home = "/home/kolja";
    shell = pkgs.zsh;
  };

####################################################################################################################### 

  nix.package = pkgs.nixUnstable;
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';
  nixpkgs.config.allowUnfree = true;

  services.tlp.enable = true;

  programs.vim.defaultEditor = true;
  programs.zsh.enable = true;
  programs.light.enable = true;

  environment.systemPackages = with pkgs; [
    curl
    vim_configurable
    git
    feh
    killall
    haskellPackages.xmonad
  ];

####################################################################################################################### 

  system.stateVersion = "20.09";
}

