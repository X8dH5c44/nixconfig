{ config, lib, pkgs, ... }:

{
  services = {
    xserver = {
      enable = true;
      layout = "de";

      libinput = {
        enable = true;
        touchpad.disableWhileTyping = true;
      };
      displayManager.defaultSession = "none+xmonad";
#     displayManager.session = [];
      windowManager.xmonad = {
        enable = true;
        enableContribAndExtras = true;
      };
    };
  };
}
