{ config, lib, pkgs, stdenv, ... }:

{
  programs.home-manager.enable = true;

  imports = (import ./programs) ++ (import ./services);

  fonts.fontconfig.enable = true;
  nixpkgs.config.allowUnfree = true;
  home.packages = with pkgs; [
    (pkgs.nerdfonts.override { fonts = [ "FiraCode" ]; })
    firefox
    gcc
    chromium
    iwd
    discord
    alacritty
    arandr
    pavucontrol
    playerctl
    pulsemixer
    picom
  ];
}
