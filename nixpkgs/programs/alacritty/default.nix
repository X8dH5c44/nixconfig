{ fontSize, pkgs, ... }:

{
  programs.alacritty = {
    enable = true;
    settings = {
      background_opacity = 0.4;
      bell = {
        animation = "EaseOutExpo";
        duration = 5;
        color = "#ffffff";
      };
      font = {
        normal = {
          family = "FiraCode Nerd Font Mono";
          style = "Regular";
        };
        bold = {
          family = "FiraCode Nerd Font Mono";
          style = "Bold";
        };
        italic= {
          family = "FiraCode Nerd Font Mono";
          style = "Italic";
        };
        bold_italic= {
          family = "FiraCode Nerd Font Mono";
          style = "Bold Italic";
        };
        size = 10.0;
      };
      colors = {
        primary = {
          background = "#282828";
          foreground = "#ebdbb2";
          dim_background = "#828482";
          dim_foreground = "#eaeaea";
        };
        cursor = {
          text= "CellBackground";
          cursor = "CellForeground";
        };
        vi_mode_cursor = {
          text= "CellBackground";
          cursor = "CellForeground";
        };
        selection = {
          text= "CellBackground";
          cursor = "CellForeground";
        };
        normal = {
          black =   "#282828";
          red =     "#cc241d";
          green =   "#98971a";
          yellow =  "#d79921";
          blue =    "#458588";
          magenta = "#b16286";
          cyan =    "#689d6a";
          white =   "#a89984";
        };
        bright = {
          black =   "#928374";
          red =     "#fb4934";
          green =   "#b8bb26";
          yellow =  "#fabd2f";
          blue =    "#83a598";
          magenta = "#d3869b";
          cyan =    "#8ec07c";
          white =   "#ebdbb2";
        };
        dim = {
          black =   "#282828";
          red =     "#cc241d";
          green =   "#98971a";
          yellow =  "#d79921";
          blue =    "#458588";
          magenta = "#b16286";
          cyan =    "#689d6a";
          white =   "#a89984";
        };
      };
      selection.save_to_clipboard = true;
      shell.program = "${pkgs.zsh}/bin/zsh";
    };
  };
}
