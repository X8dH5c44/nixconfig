{
  programs.vim = {
    enable = true;
    settings = {
      expandtab = true;
      tabstop = 2;
      shiftwidth = 2;
      number = true;
    };
    extraConfig = ''
      set cursorline
      hi LineNr ctermfg=Yellow
      hi CursorLineNr cterm=bold ctermfg=Yellow ctermbg=lightgray
      hi cursorLine cterm=bold ctermbg=lightgray
    '';
  };
}
