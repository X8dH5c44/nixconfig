{
  xsession = {
    enable = true;

    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
      extraPackages = haskellPackages: [
        haskellPackages.monad-logger
        haskellPackages.xmonad-contrib
      ];
      config = ./xmonad.hs;
    };
  };
}
