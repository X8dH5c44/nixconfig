import XMonad

import XMonad.Util.Run

import XMonad.Util.NamedScratchpad

import XMonad.Actions.Navigation2D

import XMonad.Layout.NoBorders
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.BorderResize

import Graphics.X11.ExtraTypes.XF86

import System.Exit

import qualified Data.Map as M
import qualified XMonad.StackSet as W

main = do
  xmproc <- spawnPipe("source ~/.xinitrc")
  xmonad settings

layouts = smartBorders(borderResize(emptyBSP) ||| Full)

keybinds conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
  [ ((0, xF86XK_AudioRaiseVolume), spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%"),
    ((0, xF86XK_AudioLowerVolume), spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%"),
    ((0, xF86XK_AudioMute), spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle" ),
    ((0,    xF86XK_MonBrightnessUp        ), spawn "light -A 10"       ),
    ((0,    xF86XK_MonBrightnessDown      ), spawn "light -U 10"       ),
    ((0, xK_Print), spawn "scrot '%Y-%m-%d-%H:%M:%S.png' -e 'mv $f ~/images/screenshots/'"),
    ((modm, xK_p                          ), spawn "rofi -show run"           ),
    ((modm, xK_o                          ), spawn "rofi -show drun"          ),
    ((modm, xK_Return                     ), windows W.swapMaster             ),
    ((modm, xK_t                          ), withFocused $ windows . W.sink   ),
    ((modm, xK_comma                      ), sendMessage(IncMasterN 1)        ),
    ((modm, xK_period                     ), sendMessage(IncMasterN (-1))     ),
    ((modm, xK_q               ), spawn "xmonad --recompile; xmonad --restart"),
    ((modm, xK_Right                      ), windowGo R False                 ),
    ((modm, xK_Left                       ), windowGo L False                 ),
    ((modm, xK_Up                         ), windowGo U False                 ),
    ((modm, xK_Down                       ), windowGo D False                 ),
    ((modm, xK_space                      ), sendMessage NextLayout           ),
    ((modm .|. shiftMask, xK_Right        ), windowSwap R False               ),
    ((modm .|. shiftMask, xK_Left         ), windowSwap L False               ),
    ((modm .|. shiftMask, xK_Up           ), windowSwap U False               ),
    ((modm .|. shiftMask, xK_Down         ), windowSwap D False               ),
    ((modm .|. shiftMask, xK_q            ), io(exitWith ExitSuccess)         ),
    ((modm .|. shiftMask, xK_Return       ), spawn $ XMonad.terminal conf     ),
    ((modm .|. shiftMask, xK_c            ), kill                             )]
    ++
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

settings = def {
  terminal            = "alacritty",
  modMask             = mod4Mask,
  layoutHook          = layouts,
  keys                = keybinds,
  borderWidth         = 2,
  normalBorderColor   = "#000000",
  focusedBorderColor  = "#ebdbb2"
}

