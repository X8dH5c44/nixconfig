{
  programs.zsh = {
    enable = true;
    autocd = true;
    dotDir = ".config/zsh";
    shellAliases = {
      renix = "sudo nixos-rebuild switch";
      rehome = "home-manager switch";
      setwp = "feh --bg-fill --no-fehbg";
      mkdir = "mkdir -p";
      sh-cheatsheet = "firefox https://steinbaugh.com/posts/posix.html";
    };
    enableCompletion = true;
    history = {

    };
    defaultKeymap = "viins";
    initExtraBeforeCompInit = ''
      autoload -Uz compinit
      zstyle ':completion:*' menu select
      zmodload zsh/complist
      compinit
      _comp_options+=(globdots)
    '';
  };
}
