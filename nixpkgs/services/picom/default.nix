{
  services.picom = {
    enable = true;
    blur = true;
    experimentalBackends = true;
    extraOptions = ''
      blur-method = "gaussian";
      blur-size = "10";
      blur-deviation = "5";
    '';
  };
}
